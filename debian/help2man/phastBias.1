.TH PHASTBIAS "1" "May 2016" "phastBias 1.4" "User Commands"
.SH NAME
phastBias \- Identify regions of the alignment which are affected by gBGC,
.SH SYNOPSIS
The alignment file can be in any of several file formats (see
\fB\-\-msa\-format\fR).  The neutral model must be in the .mod format
produced by the phyloFit program.  The foreground_branch should
identify a branch of the tree (internal branches can be named
with tree_doctor \fB\-\-name\-ancestors\fR).
.SH DESCRIPTION
Identify regions of the alignment which are affected by gBGC,
indicated by a cluster of weak\-to\-strong (A/T \-> G/C) substitutions
amidst a deficit of strong\-to\-weak substitutions on a particular
branch of the tree.  The regions are identified by a phylo\-HMM
with four states: neutral, conserved, neutral with gBGC, and
conserved with gBGC.
.SS OUTPUT:
phastBias produces a wig file with scores for every position in the
alignment indicating the probability of being in one of the gBGC
states.  It can also produce gBGC tracts by thresholding this
probability at 0.5, or a matrix of probabilities for all four states.
See OUTPUT OPTIONS below.
.SH OPTIONS
.SS GENERAL OPTIONS:
.HP
\fB\-\-help\fR,\-h
Print this help message.
.PP
TUNING PARAMETER OPTIONS:
.IP
gBGC PARAMETERS:
.HP
\fB\-\-bgc\fR <B>
.TP
The B parameter describes the strength of gBGC.
It must be > 0.
.IP
Too low of a value may yield false positives, as the gBGC model
becomes indistinguishable from the non\-gBGC model.
.IP
Default: 3
.HP
\fB\-\-estimate\-bgc\fR <0|1>
Use "\-\-estimate\-bgc 1" to estimate B by maximum likelihood.
Default: 0
.HP
\fB\-\-bgc\-exp\-length\fR <length>
.TP
Set the prior expected length of gBGC tracts.
This is equivalent to
.IP
1/alpha in the parametrization defined by Capra et al, where
alpha is the rate out of gBGC states.
.IP
Default: 1000
.HP
\fB\-\-estimate\-bgc\-exp\-length\fR <0|1>
Use "\-\-estimate\-bgc\-exp\-length 1" to estimate this parameter by an
.IP
expectation\-maximization algorithm.
.IP
Default: 0
.HP
\fB\-\-bgc\-target\-coverage\fR <coverage>
.IP
Set the prior for gBGC tract coverage (as a fraction between 0 and 1).
.IP
This is represented in the model as beta/(alpha+beta), where beta
is the rate into the gBGC state, and alpha is the rate out of the
gBGC state.
.IP
Default: 0.01
.HP
\fB\-\-estimate\-bgc\-target\-coverage\fR <0|1>
Use "\-\-estimate\-bgc\-target\-coverage 0" to hold this parameter constant.
Default: 1 (This is the only parameter estimated by default.)
.SS CONSERVATION PARAMETERS:
.PP
Note: it is not recommended to tune these parameters with phastBias.
.PP
Rather, phastCons may be used to determine the best values for rho
and the transition rates into/out of conserved elements.  See
phastCons \fB\-\-help\fR and the phastCons HOWTO (available online) to learn
about tuning these parameters.
.HP
\fB\-\-rho\fR <rho>
.TP
Set the scaling factor for branch lengths in conserved states.
Rho should
.IP
be between 0 and 1.
.IP
Default: 0.31
.HP
\fB\-\-cons\-exp\-length\fR <length>
.TP
Set the prior expected length of conserved elements.
This parameter is
.IP
held constant; if you want to tune it, it is recommended to do this
with the phastCons program under a non\-gBGC model (see the
\fB\-\-expected\-length\fR option in phastCons).
Default: 45
.HP
\fB\-\-cons\-target\-coverage\fR <cov>
.IP
Set the prior for coverage of conserved elements (as a fraction
between 0 and 1).
Like the \fB\-\-cons\-exp\-length\fR above, this parameter
is also held constant, but can be tuned with phastCons (see
phastCons \fB\-\-transitions\fR).
Default: 0.3
.SS OTHER PARAMETERS:
.HP
\fB\-\-scale\fR <scale>
Set an overall scaling factor for the branch lengths in all states.
Default: 1
.HP
\fB\-\-estimate\-scale\fR <0|1>
.IP
Rescale the branches in all states by a scaling factor determined by
.IP
maximum likelihood (initialized by \fB\-\-scale\fR above).
Default: 0
.HP
\fB\-\-eqfreqs\-from\-msa\fR <0|1>
.IP
Reset equilibrium frequencies of A,C,G,T based on frequencies observed
in the alignment.
Otherwise will not be altered from input model.
Default: 1
.SS OUTPUT OPTIONS
.HP
\fB\-\-output\-tracts\fR <file.gff>
.IP
Print a GFF file identifying all regions with posterior probability of
being in a gBGC state > 0.5.
.HP
\fB\-\-posteriors\fR <none|wig|full>
.IP
Use this option to control posterior probability output, which is
written to stdout.
"none" implies do not output anything; wig outputs
a standard fixed\-step wiggle file giving the probability that each
base is assigned to a gBGC state; "full" outputs a table with five
columns.  The first column is the coordinate (1\-based relative to
the first sequence in the alignment), followed by the probabilities
of each of the four states: neutral, conserved, gBGC neutral,
gBGC conserved.
.IP
Default: wig
.HP
\fB\-\-output\-mods\fR <output_root>
.IP
Print out the tree models for all four states to <output_root>.cons.mod,
<output_root>.neutral.mod, <output_root>.gBGC_cons.mod, and
<output_root>.gBGC_neutral.mod.
.HP
\fB\-\-informative\-fn\fR,\-i <file.gff>
.IP
Print a GFF containing regions of the alignment which are informative
for gBGC. Note: only works properly if foreground branch is a single
branch (not a group of branches).
.HP
\fB\-\-informative\-only\fR,\-o
.IP
(To be used with \fB\-\-informative\-fn\fR). Print the informative regions, then
quit.
.SH SEE ALSO
Capra JA, Hubisz MJ, Kostka D, Pollard KS, Siepel A: A Model\-Based Analysis
of GC\-Biased Gene Conversion in the Human and Chimpanzee Genomes.
(Manuscript in submission).
