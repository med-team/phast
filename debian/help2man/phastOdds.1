.TH PHASTODDS "1" "May 2016" "phastOdds 1.4" "User Commands"
.SH NAME
phastOdds \- Compute log-odds scores based on two phylogenetic models or phylo-HMMs,
.SH DESCRIPTION
Compute log\-odds scores based on two phylogenetic models or phylo\-HMMs,
one for features of interest (e.g., coding exons, conserved regions)
and one for background.  Will either (1) compute a score for each
feature in an input set, and output the same set of features with
scores; or (2) output a separate score for each position in fixed\-step
WIG format (http://genome.ucsc.edu/goldenPath/help/wiggle.html); or (3)
compute scores in a sliding window of designated size, and output a
three\-column file, with the index of the center of each window followed
by the score for that window on the positive strand, then the score for
that window on the negative strand.  The default is to assume a
reference sequence alignment, with the reference sequence appearing
first; feature coordinates are assumed to be defined with respect to
the reference sequence (see \fB\-\-refidx\fR).
.SH SYNOPSIS
.B phastOdds
[OPTIONS] 
\fB\-\-background\-mods\fR <bmods> [\-\-background\-hmm <bhmm>]
\fB\-\-feature\-mods\fR <fmods> [\-\-feature\-hmm <fhmm>]
( \fB\-\-features\fR <feats> | \fB\-\-window\fR <size> )
<alignment>
.PP
Arguments <bmods> and <fmods> should be comma\-delimited lists of
phylogenetic models in .mod format (as produced by phyloFit), <feats>
may be in GFF, BED, or genepred format, and <alignment> may be in FASTA
format or an alternative format specified by \fB\-\-msa\-format\fR.  HMM files
should be in the format used by exoniphy.
.SH EXAMPLE
(See below for more details on options)
.PP
1. Compute conservation scores for features in a GFF file, based on a

model for conserved sites (conserved.mod) vs. a model of neutral
evolution (neutral.mod).  (These models may be estimated with
phyloFit or phastCons.)
.IP
phastOdds \fB\-\-background\-mods\fR neutral.mod \fB\-\-feature\-mods\fR conserved.mod
\fB\-\-features\fR features.gff alignment.fa > scores.gff
.PP
Features could alternatively be specified in BED or genepred format
(format will be auto\-recognized).  The program can be made to
produce BED\-formatted output with \fB\-\-output\-bed\fR.
.PP
2. Compute conservation scores in a sliding window of size 100.
.IP
phastOdds \fB\-\-background\-mods\fR neutral.mod \fB\-\-feature\-mods\fR conserved.mod
\fB\-\-window\fR 100 alignment.fa > scores.dat
.PP
(Window is advanced one site at a time.
Window boundaries are
defined in the coordinate frame of the multiple alignment, but
center coordinates are converted to the frame of the reference
sequence as they are output.)
.PP
3. Compute a "coding potential" score for features in a BED file, based
on a phylo\-HMM for coding regions versus a phylo\-HMM for noncoding
DNA, with states for conserved and nonconserved sequences.
.IP
phastOdds \fB\-\-background\-mods\fR codon1.mod,codon2.mod,codon3.mod
\fB\-\-background\-hmm\fR coding.hmm
\fB\-\-feature\-mods\fR neutral.mod,conserved\-noncoding.mod
\fB\-\-feature\-hmm\fR noncoding.hmm
\fB\-\-features\fR features.bed \fB\-\-output\-bed\fR alignment.fa > scores.bed
.SH OPTIONS
.HP
\fB\-\-background\-mods\fR, \fB\-b\fR <backgd_mods>
.IP
(Required) Comma\-delimited list of tree model (*.mod) files for
background.  If used with \fB\-\-background\-hmm\fR, order of models must
correspond to order of states in HMM.
.HP
\fB\-\-background\-hmm\fR, \fB\-B\fR <backgd.hmm>
.TP
HMM for background.
If there is only one backgound tree
.IP
model, a trivial (single\-state) HMM will be assumed.
.HP
\fB\-\-feature\-mods\fR, \fB\-f\fR <feat_mods>
(Required) Comma\-delimited list of tree model (*.mod) files for
features.  If used with \fB\-\-feature\-hmm\fR, order of models must
correspond to order of states in HMM.
.HP
\fB\-\-feature\-hmm\fR, \fB\-F\fR <feat.hmm>
HMM for features.
If there is only one tree model for
features, a trivial (single\-state) HMM will be assumed.
.HP
\fB\-\-features\fR, \fB\-g\fR <feats.gff>
.IP
(Required unless \fB\-w\fR or \fB\-y\fR) File defining features to be scored
(GFF, BED, or genepred).
.HP
\fB\-\-window\fR, \fB\-w\fR <size>
(Can be used instead of \fB\-g\fR or \fB\-y\fR) Compute scores in a sliding
window of the specified size.
.HP
\fB\-\-base\-by\-base\fR, \fB\-y\fR
.IP
(Can be used instead of \fB\-g\fR or \fB\-y\fR) Output base\-by\-base scores, in
the coordinate frame of the reference sequence (or of the sequence
specified by \fB\-\-refidx\fR).  Output is in fixed\-step WIG format
(http://genome.ucsc.edu/goldenPath/help/wiggle.html).  This option
can only be used with individual phylogenetic models, not with sets
of models and a (nontrivial) HMM.
.HP
\fB\-\-window\-wig\fR, \fB\-W\fR <size>
.IP
(Can be used instead of \fB\-g\fR or \fB\-y\fR) Like \fB\-\-window\fR but outputs scores
in fixed\-step WIG format, as with \fB\-\-base\-by\-base\fR.  Scores for the
positive strand only are output.
.HP
\fB\-\-msa\-format\fR, \fB\-i\fR <type>
.TP
Input format for alignment.
May be FASTA, PHYLIP, MPM, SS, or
.IP
MAF (default is to guess format from file contents).
.HP
\fB\-\-refidx\fR, \fB\-r\fR <ref_seq>
Index of reference sequence for coordinates.
Use 0 to
indicate the coordinate system of the alignment as a whole.
Default is 1, for first sequence.
.HP
\fB\-\-output\-bed\fR, \fB\-d\fR
.IP
(For use with \fB\-g\fR) Generate output in bed format rather than GFF.
.HP
\fB\-\-verbose\fR, \fB\-v\fR
Verbose mode.
Print messages to stderr describing what the
program is doing.
.HP
\fB\-\-help\fR, \fB\-h\fR
.IP
Print this help message.
