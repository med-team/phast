.TH PHYLOP "1" "May 2016" "phyloP 1.4" "User Commands"
.SH NAME
phyloP \- Compute conservation or acceleration p-values based on an alignment and
The phylogenetic model must be in the .mod format produced by the
phyloFit program.  The alignment file can be in any of several file
formats (see \fB\-\-msa\-format\fR).  No alignment is required with the \fB\-\-null\fR
option.
.SH DESCRIPTION
Compute conservation or acceleration p\-values based on an alignment and
a model of neutral evolution.  Will also compute p\-values of
conservation/acceleration in a subtree and in its complementary
supertree given the whole tree (see \fB\-\-subtree\fR).  P\-values can be
produced for entire input alignments (the default), pre\-specified
intervals within an alignment (see \fB\-\-features\fR), or individual sites
(see \fB\-\-wig\-scores\fR and \fB\-\-base\-by\-base\fR).
.PP
The default behavior is to compute a null distribution for the total
number of substitutions from the tree model, an estimate of the number
of substitutions that have actually occurred, and the p\-value of this
estimate wrt the null distribution.  These computations are performed
as described by Siepel, Pollard, and Haussler (2006).  In addition to
the SPH method, phyloP can compute p\-values or
conservation/acceleration scores using a likelihood ratio test
(\fB\-\-method\fR LRT), a score\-based test (\fB\-\-method\fR SCORE), or a procedure
similar to that used by GERP (Cooper et al., 2005) (\fB\-\-method\fR GERP).
These alternative methods are currently supported only with
\fB\-\-base\-by\-base\fR, \fB\-\-wig\-scores\fR, or \fB\-\-features\fR.
.PP
The main advantage of the SPH method is that it can provide a complete
and exact description of distributions over numbers of substitutions.
However, simulation experiments suggest that the LRT and SCORE methods
have somewhat better power than SPH for identifying selection,
especially when the expected number of substitutions is small (e.g.,
with short branch lengths and/or short intervals/individual sites).
These two methods are also faster.  They are generally similar to one
another in power, but in many cases SCORE is considerably faster than
LRT.  On the other hand, SCORE appears to have slightly less power than
LRT at low false positive rates, i.e., for cases of extreme selection.
Thus, when using \fB\-\-base\-by\-base\fR, \fB\-\-wig\-scores\fR, or \fB\-\-features\fR, LRT is
recommended for most purposes, but SCORE is a good alternative if speed
is an issue.
When computing p\-values with the SPH method, the default is to use the
posterior expected number of substitutions as an estimate of the actual
number.  This is a conservative estimate, because it is biased toward
the mean of the null distribution by the prior.  These p\-values can be
made less conservative with \fB\-\-fit\-model\fR and more conservative with
\fB\-\-confidence\-interval\fR (see below).
.SH EXAMPLE
.PP
1. Using the SPH method, compute and report p\-values of conservation
and acceleration for a given alignment with respect to a neutral model
of evolution.  Estimated numbers of substitutions are also reported.
.IP
phyloP neutral.mod alignment.fa > report.txt
.PP
The file neutral.mod could be produced by running phyloFit on data from
ancestral repeats or fourfold degenerate sites with an appropriate tree
topology and substitution model.
.PP
2. Compute and report p\-values of conservation and acceleration for a
particular subtree of interest (using SPH).
.IP
phyloP \fB\-\-subtree\fR human\-mouse_lemur neutral.mod alignment.fa > report.txt
.PP
Here human\-mouse_lemur denote the most recent common ancestor of human
and mouse_lemur, which is the node that defines the primate clade in
this phylogeny.  The tree_doctor program with the \fB\-\-name\-ancestors\fR
option can be used to assign names to ancestral nodes of the tree.
.PP
3. Describe the complete null distribution over the number of
substitutions for a 10bp alignment given the specified neutral model
(using SPH).
.IP
phyloP \fB\-\-null\fR 10 neutral.mod > null.txt
.PP
A two\-column table is produced with numbers of substitutions and their
probabilities, up to an appropriate upper limit.
.PP
4. Describe the complete posterior distribution over the number of
substitutions in a given alignment (using SPH).
.IP
phyloP \fB\-\-posterior\fR neutral.mod alignment.fa > posterior.txt
.PP
5. Compute conservation scores (\fB\-log10\fR p\-values) for each site in an
alignment and output them in the fixed\-step wig format (see
http://genome.ucsc.edu/goldenPath/help/wiggle.html).  Use the
likelihood ratio test (LRT) method.
.IP
phyloP \fB\-\-wig\-scores\fR \fB\-\-method\fR LRT neutral.mod alignment.fa > scores.wig
.PP
The \fB\-\-mode\fR option can be used instead to produce acceleration scores
(ACC), scores of nonneutrality (NNEUT), or scores that summarize
conservation and acceleration (CONACC).  The \fB\-\-base\-by\-base\fR option can
be used to output additional statistics of interest (estimated scale
factors, log10 likelihood ratios, etc.).  As discussed above, several
arguments to \fB\-\-method\fR are possible.
.PP
6. Similarly, compute scores describing lineage\-specific conservation
in primates.
.IP
phyloP \fB\-\-wig\-scores\fR \fB\-\-method\fR LRT \fB\-\-subtree\fR human\-mouse_lemur
neutral.mod alignment.fa > scores.wig
.PP
7. Compute conservation p\-values and associated statistics for each
element in a BED file.  This time use a score test and allow for
acceleration as well as conservation, flagging elements under
acceleration by making their p\-values negative (CONACC mode).
.IP
phyloP \fB\-\-features\fR elements.bed \fB\-\-method\fR SCORE \fB\-\-mode\fR CONACC
neutral.mod alignment.fa > element\-scores.txt
.PP
This option can also be used with \fB\-\-subtree\fR.
The \fB\-\-gff\-scores\fR option
can be used to output the original features in GFF format with scores
equal to \fB\-log10\fR p.  Note that the input file can be in GFF instead of BED
format.
.SH OPTIONS
.HP
\fB\-\-msa\-format\fR, \fB\-i\fR FASTA|PHYLIP|MPM|MAF|SS
.IP
Alignment format (default is to guess format from file contents).
.HP
\fB\-\-method\fR, \fB\-m\fR SPH|LRT|SCORE|GERP
.IP
Method used to compute p\-values or conservation/acceleration scores
(Default SPH).  The likelihood ratio test (LRT) and score test
(SCORE) compare an alternative model having a free scale parameter
with the given neutral model, or, if \fB\-\-subtree\fR is used, an
alternative model having free scale parameters for the supertree
and subtree with a null model having a single free scale parameter.
P\-values are computed by comparing test statistics with asymptotic
chi\-square null distributions.  The GERP\-like method (GERP)
estimates the number of "rejected substitutions" per base by
comparing the (per\-site) maximum likelihood expected number of
substitutions with the expected number under the neutral model.
Currently LRT, SCORE, and GERP can be used only with
\fB\-\-base\-by\-base\fR, \fB\-\-wig\-scores\fR, or \fB\-\-features\fR.
.HP
\fB\-\-wig\-scores\fR, \fB\-w\fR
.IP
Compute separate p\-values per site, and then compute site\-specific
conservation (acceleration) scores as \fB\-log10\fR(p).  Output base\-by\-base
scores in fixed\-step wig format, using the coordinate system of the
reference sequence (see \fB\-\-refidx\fR).  In GERP mode, outputs rejected
substitutions per site instead of \fB\-log10\fR p\-values.
.HP
\fB\-\-base\-by\-base\fR, \fB\-b\fR
.IP
Like \fB\-\-wig\-scores\fR, but outputs multiple values per site, in a
method\-dependent way.  With 'SPH', output includes mean and
variance of posterior distribution, with LRT and SCORE it
includes the estimated scale factor(s) and test statistics, and
with GERP it includes the estimated numbers of neutral,
observed, and rejected substitutions, along with the number of
species available at each site.
.HP
\fB\-\-refidx\fR, \fB\-r\fR <refseq_idx>
.IP
(for use with \fB\-\-wig\-scores\fR or \fB\-\-base\-by\-base\fR) Use coordinate frame
of specified sequence in output.  Default value is 1, first
sequence in alignment; 0 indicates coordinate frame of entire
multiple alignment.
.HP
\fB\-\-mode\fR, \fB\-o\fR CON|ACC|NNEUT|CONACC
.IP
(For use with \fB\-\-wig\-scores\fR, \fB\-\-base\-by\-base\fR, or \fB\-\-features\fR) Whether
to compute one\-sided p\-values so that small p (large \fB\-log10\fR p)
indicates unexpected conservation (CON; the default) or
acceleration (ACC); or two\-sided p\-values such that small p
indicates an unexpected departure from neutrality (NNEUT).  The
fourth option (CONACC) uses positive values (p\-values or scores) to
indicate conservation and negative values to indicate acceleration.
In GERP mode, CON and CONACC both report the number of rejected
substitutions R (which may be negative), while ACC reports \fB\-R\fR, and
NNEUT reports abs(R).
.HP
\fB\-\-features\fR, \fB\-f\fR <file>
.IP
Read features from <file> (GFF or BED format) and output a
table of p\-values and related statistics with one row per
feature.  The features are assumed to use the coordinate frame
of the first sequence in the alignment.  Not for use with
\fB\-\-null\fR or \fB\-\-posterior\fR.  See also \fB\-\-gff\-scores\fR.
.HP
\fB\-\-gff\-scores\fR, \fB\-g\fR
.IP
(For use with features)
Instead of a table, output a GFF and
assign each feature a score equal to its \fB\-log10\fR p\-value.
.HP
\fB\-\-subtree\fR, \fB\-s\fR <node\-name>
.IP
(Not available in GERP mode) Partition the tree into the subtree
beneath the node whose name is given and the complementary
supertree, and consider conservation/acceleration in the subtree
given the supertree.  The branch above the specified node is
included with the subtree.  Thus, given the tree
"((human,chimp)primate,(mouse,rat)rodent)", the option "\-\-subtree
primate" will create one partition consisting of human, chimp, and
the branch leading to them, and another partition consisting of the
rest of the tree; "\-\-subtree human" will create one partition
consisting only of human and the branch leading to it and another
partition consisting of the rest of the tree.  In 'SPH' mode, a
reversible substitution model is assumed.
.HP
\fB\-\-branch\fR, \fB\-B\fR <node\-name(s)>
.IP
(Not available in GERP or SPH mode).
Like subtree, but partitions
the tree into the set of named branches (each named by its child
node), and all the remaining branches.  Then tests for conservation/
acceleration in the set of named branches relative to the others.
The argument is a comma\-delimited list of child nodes.
.HP
\fB\-\-chrom\fR, \fB\-N\fR <name>
.IP
(Optionally use with \fB\-\-wig\-scores\fR or \fB\-\-base\-by\-base\fR) Chromosome
name for wig output.  Default is root of multiple alignment
filename.
.HP
\fB\-\-log\fR, \fB\-l\fR <fname>
.IP
Write log to <fname> describing details of parameter optimization.
Useful for debugging.  (Warning: may produce large file.)
.HP
\fB\-\-seed\fR, \fB\-d\fR <seed>
.IP
Provide a random number seed, should be an integer >=1.
Random numbers are used in some cases to generate starting values for
optimization.  If not specified will use a seed based on the
current time.
.HP
\fB\-\-no\-prune\fR,\-P
.IP
Do not prune species from tree which are not in alignment.
Rather,
treat these species as having missing data in the alignment.
Missing
data does have an effect on the results when \fB\-\-method\fR SPH is used.
.HP
\fB\-\-help\fR, \fB\-h\fR
.IP
Produce this help message.
.SS Options for SPH mode only
.HP
\fB\-\-null\fR, \fB\-n\fR <nsites>
Compute just the null (prior) distribution of the number of
substitutions, as defined by the tree model and the given
number of sites, and output as a table.  The 'alignment'
argument will be ignored.  If used with \fB\-\-subtree\fR, the joint
distribution over the number of substitutions in the specified
supertree and subtree will be output instead.
.HP
\fB\-\-posterior\fR, \fB\-p\fR
Compute just the posterior distribution of the number of
substitutions, given the alignment and the model, and output
as a table.  If used with \fB\-\-subtree\fR, the joint distribution
over the number of substitutions in the specified supertree
and subtree will be output instead.
.HP
\fB\-\-fit\-model\fR, \fB\-F\fR
.IP
Fit model to data before computing posterior distribution, by
estimating a scale factor for the whole tree or (if \fB\-\-subtree\fR)
separate scale factors for the specified subtree and supertree.
Makes p\-values less conservative.  This option has no effect with
\fB\-\-null\fR and currently cannot be used with \fB\-\-features\fR.  It can be
used with \fB\-\-wig\-scores\fR and \fB\-\-base\-by\-base\fR.
.HP
\fB\-\-epsilon\fR, \fB\-e\fR <val>
.IP
(Default 1e\-10 or 1e\-6 if \fB\-\-wig\-scores\fR or \fB\-\-base\-by\-base\fR) Threshold
used in truncating tails of distributions; tail probabilities less
than this value are discarded.  To get accurate p\-values smaller
than 1e\-10, this option will need to be used, at some cost in
speed.  Note that truncation affects only *right* tails, not left
tails, so it should be an issue only with p\-values of acceleration.
.HP
\fB\-\-confidence\-interval\fR, \fB\-c\fR <val>
.IP
Allow for uncertainty in the estimate of the actual number of
substitutions by using a (central) confidence interval about the
mean of the specified size (0 < val < 1).  To be conservative, the
maximum of this interval is used when computing a p\-value of
conservation, and the minimum is used when computing a p\-value of
acceleration.  The variance of the posterior is computed
exactly, but the confidence interval is based on the assumption
that the combined distribution will be approximately normal (true
for large numbers of sites by central limit theorem).
.HP
\fB\-\-quantiles\fR, \fB\-q\fR
.IP
(For use with \fB\-\-null\fR or \fB\-\-posterior\fR) Report quantiles of
distribution rather than whole distribution.
.SH SEE ALSO
.PP
Cooper GM, Stone EA, Asimenos G, NISC Comparative Sequencing Program,
Green ED, Batzoglou S, Sidow A. Distribution and intensity of
constraint in mammalian genomic sequence.  Genome Res. 2005
15(7):901\-13.
.PP
Siepel A, Pollard KS, and Haussler D. New methods for detecting
lineage\-specific selection. In Proceedings of the 10th International
Conference on Research in Computational Molecular Biology (RECOMB
2006), pp. 190\-205.
