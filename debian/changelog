phast (1.7+dfsg-2) unstable; urgency=medium

  * d/rules: use $(MAKE) to avoid interference with the jobserver.
  * make.patch: new: fix build parallelism issues.
    Several makefiles collide on editing the libphast.a, so this patch
    blocks the parallelism on that target. (Closes: #1091563)
  * gcc-14.patch: declare forwarding unneeded.
  * d/patches/*: normalize the last-update timestamp.
  * hardening.patch: add support for CPPFLAGS.
    This fixes remaining issues left in binaries hardening.

 -- Étienne Mollier <emollier@debian.org>  Sun, 05 Jan 2025 21:16:07 +0100

phast (1.7+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 06 Dec 2024 21:33:23 +0100

phast (1.6+dfsg-6) unstable; urgency=medium

  * gcc-14.patch: fix build failure with gcc 14. (Closes: #1075383)
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.0.
  * d/p/clapack.patch: convert e acute to UTF-8.

 -- Étienne Mollier <emollier@debian.org>  Wed, 31 Jul 2024 22:24:23 +0200

phast (1.6+dfsg-5) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix FTBFS due to -Werror-implicit-function-declaration
    (Closes: #1066451)
  * Added B-D on lapacke-dev for porting functions to lapack equivalents

 -- Nilesh Patra <nilesh@iki.fi>  Tue, 26 Mar 2024 15:19:11 +0530

phast (1.6+dfsg-4) unstable; urgency=medium

  [ Andreas Tille ]
  * s/libatlas-base-dev/liblapacke-dev/ in Build-Depends to use LAPACKE
    instead of clapack
  * Provide alternative Build-Depends to liblapack-dev and libblas-dev
  * Standards-Version: 4.6.2 (routine-update)
  * Upstream email removed from d/copyright since its bouncing

  [ Sébastien Villemot ]
  * Complete migration away from ATLAS
    Closes: #1056681

 -- Andreas Tille <tille@debian.org>  Wed, 29 Nov 2023 10:25:18 +0100

phast (1.6+dfsg-3) unstable; urgency=medium

  * Port to pcre2
    Closes: #1000000, #1000163

 -- Andreas Tille <tille@debian.org>  Fri, 19 Nov 2021 13:17:54 +0100

phast (1.6+dfsg-2) unstable; urgency=medium

  * Build-Depends: s/libpcre3-dev/libpcre2-dev/
    Closes: #1000000
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Thu, 18 Nov 2021 14:52:47 +0100

phast (1.6+dfsg-1) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * watch file standard 4 (routine-update)
  * Build-Depends: libf2c2-dev, libatlas-base-dev

 -- Andreas Tille <tille@debian.org>  Mon, 30 Aug 2021 21:53:58 +0200

phast (1.5+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add patch to fix tests on arm64 (Closes: #911983)

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 03 Oct 2020 12:51:30 +0530

phast (1.5+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fix homepage
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Run part or the test suite

 -- Andreas Tille <tille@debian.org>  Fri, 19 Oct 2018 09:10:34 +0200

phast (1.4+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #875555)

 -- Andreas Tille <tille@debian.org>  Tue, 12 Sep 2017 10:11:38 +0200
